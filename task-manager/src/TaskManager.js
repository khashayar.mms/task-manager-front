import React, { Component } from 'react';
import { days, getTodaysIndex } from './utils';
import { Grid, Button } from 'semantic-ui-react';
import TasksGroup from './TasksGroup';
import './TaskManager.css';

export default class TaskManager extends Component {

  constructor(props) {
    super(props);

    const todayIndex = getTodaysIndex();
    const previousDayIndex = (todayIndex - 1 + 7) % 7;
    const nextDayIndex = (todayIndex + 1) % 7;

    this.state = {
      days: [previousDayIndex, todayIndex, nextDayIndex]
    };
  }

  prevDays = () => {
    const days = this.state.days.map((dayNumber) => {
      return (dayNumber - 1 + 7) % 7;
    });

    this.setState({ days });
  }

  nextDays = () => {
    const days = this.state.days.map((dayNumber) => {
      return (dayNumber + 1) % 7;
    });

    this.setState({ days });
  }

  render() {
    const showingDays = this.state.days;
    const plans = showingDays.map((dayNumber) => {
      const dayName = days[dayNumber];

      return (
        <TasksGroup weekday={dayName} key={dayName} />
      );
    });

    return (
      <Grid centered relaxed divided>
        <Grid.Column width={1}>
          <Button icon="chevron left" color="grey" onClick={this.prevDays} size="large" circular basic />
        </Grid.Column>
        {plans}
        <Grid.Column width={1}>
          <Button icon="chevron right" color="grey" onClick={this.nextDays} size="large" circular basic />
        </Grid.Column>
      </Grid>
    );
  }
}