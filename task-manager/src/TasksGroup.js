import React, { Component } from 'react';
import { Grid, Label, Button } from 'semantic-ui-react';
import Task from './Task';
import NewTaskModal from './NewTaskModal';
import { days, getTodaysIndex } from './utils';

import axios from 'axios';

export default class TasksGroup extends Component {

    constructor(props) {
        super(props);

        const tasks = [];

        this.state = {
            tasks,
            showModal: false
        };
    }

    componentDidMount() {
        axios.get(`/tasks/${this.props.weekday}/`).then(res => {
            const tasks = [];
            for (let task of res.data.results) {
                tasks.push(task);
            }

            this.findOverlaps(tasks);
        });
    }

    addTask = (task) => {
        const tasks = this.state.tasks

        let index = 0;
        while (index < tasks.length && tasks[index]['start_time'] < task['start_time']) {
            index++;
        }
        tasks.splice(index, 0, task);

        this.findOverlaps(tasks);
    }

    removeTask = (taskID) => {
        const tasks = this.state.tasks;
        const index = tasks.indexOf(taskID);
        tasks.splice(index, 1);
        this.findOverlaps(tasks);
    }

    showNewTaskModal = () => {
        this.setState({ showModal: true });
    }

    closeModal = () => {
        this.setState({ showModal: false });
    }

    findOverlaps(tasks) {
        for (let i = 0; i < tasks.length - 1; i++) {
            if (tasks[i]['end_time'] > tasks[i + 1]['start_time']) {
                tasks[i].overlaps = true;
                tasks[i + 1].overlaps = true;
            } else if (tasks[i].overlaps !== true) {
                tasks[i].overlaps = false;
            }
        }

        if (tasks.length > 0 && tasks[tasks.length - 1].overlaps !== true) {
            tasks[tasks.length - 1].overlaps = false;
        }

        this.setState({ tasks });
    }

    render() {
        const today = getTodaysIndex();
        const itsToday = days[today] === this.props.weekday;
        const color = itsToday ? 'teal' : 'grey';

        const tasks = this.state.tasks.map((task) => {
            return (
                <Grid.Column key={task.task_id}>
                    <Task id={task.task_id} description={task.description} start={task.start_time} end={task.end_time} overlaps={task.overlaps} remove={this.removeTask} />
                </Grid.Column>
            );
        });

        return (
            <Grid.Column width={4}>
                <Grid columns={1}>
                    <Grid.Column>
                        <Label size="huge" color={color} active={itsToday} basic>
                            {this.props.weekday}
                        </Label>
                        <Button icon="add" floated="right" onClick={this.showNewTaskModal} circular basic />
                        <NewTaskModal day={this.props.weekday} open={this.state.showModal} close={this.closeModal} addTask={this.addTask} />
                    </Grid.Column>
                    {tasks}
                </Grid>
            </Grid.Column>
        );
    }
}