const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

function getTodaysIndex(index) {
    return (new Date().getDay() - 1 + 7) % 7;
}

module.exports = {
    days,
    getTodaysIndex
};