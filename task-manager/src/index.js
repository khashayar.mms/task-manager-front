import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import TaskManager from './TaskManager';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<TaskManager />, document.getElementById('root'));
registerServiceWorker();
