import React, { Component } from 'react';
import { Modal, Button, Form } from 'semantic-ui-react';

import axios from 'axios';

export default class NewTaskModal extends Component {

    constructor(props) {
        super(props);

        this.state = {
            title: "",
            start: "",
            end: ""
        };
    }

    titleChange = (event) => {
        this.setState({ title: event.target.value });
    }

    startChange = (event) => {
        this.setState({ start: event.target.value });
    }

    endChange = (event) => {
        this.setState({ end: event.target.value });
    }

    saveTask = () => {
        axios.post('/tasks/', {
            description: this.state.title,
            weekday: this.props.day,
            start_time: this.state.start,
            end_time: this.state.end
        }).then(res => {
            if (res.status === 201) {
                this.props.addTask(res.data);
            } else if (res.status === 400) {
                console.log(res);
            }
        });
        this.props.close();
    }

    render() {
        return (
            <Modal dimmer="blurring" size="mini" open={this.props.open} onClose={this.props.close} closeIcon>
                <Modal.Header>Create a New Task on {this.props.day}!</Modal.Header>
                <Modal.Content>
                    <Modal.Description>
                        <Form>
                            <Form.Field>
                                <label>Title</label>
                                <input type="text" placeholder="Task Title" onChange={this.titleChange} />
                            </Form.Field>
                            <Form.Group widths={2}>
                                <Form.Field>
                                    <label>Starting time</label>
                                    <input type="time" placeholder="Start time" onChange={this.startChange} />
                                </Form.Field>
                                <Form.Field>
                                    <label>Ending time</label>
                                    <input type="time" placeholder="Ending time" onChange={this.endChange} />
                                </Form.Field>
                            </Form.Group>
                        </Form>
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                    <Button negative icon='close' labelPosition='right' content="Cancel" onClick={this.props.close} basic />
                    <Button positive icon='checkmark' labelPosition='right' content="Save" onClick={this.saveTask} basic />
                </Modal.Actions>
            </Modal>
        );
    }
}