import React, { Component } from 'react';
import { Card, Icon, Button } from 'semantic-ui-react';

import axios from 'axios';

export default class Task extends Component {

    handleRemove = () => {
        axios.delete('/tasks/', {
            data: {
                task_id: this.props.id
            }
        }).then(res => {
            if (res.status === 404) {
                console.log(res);
            }
        });
        this.props.remove(this.props.id);
    };

    render() {
        const className = this.props.overlaps ? "overlapping" : "";
        return (
            <Card className={className} raised>
                <Card.Content>
                    <Card.Header>
                        {this.props.description}
                    </Card.Header>
                </Card.Content>
                <Card.Content>
                    <Card.Description>
                        <Icon name="clock" color="green" /> Starts at {this.props.start}
                        <br />
                        <Icon name="clock" color="red" /> Ends at {this.props.end}
                    </Card.Description>
                </Card.Content>
                <Button icon="trash" attached="bottom" color="red" content="Remove" onClick={this.handleRemove} basic />
            </Card>
        );
    }
}